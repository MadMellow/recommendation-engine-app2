/**
 * @author Saurabh
 * @email saurabh.mishra@embibe.com
 * @create 
 * @modify 
 * @desc [description]
 */

// Main
const BASE_ROUTE = '/recommendation-engine-app/v1'
const HEALTH_CHECK_ROUTE = '/health'
const POSTS_ROUTE = '/post'
const DOCS_ROUTE = '/docs'

// Post
const RELATED_POSTS_ROUTE = '/related/:postId'


module.exports = {
  BASE_ROUTE,
  HEALTH_CHECK_ROUTE,
  POSTS_ROUTE,
  DOCS_ROUTE,
  RELATED_POSTS_ROUTE
}