/**
 * @author Saurabh
 * @email saurabh.mishra@embibe.com
 * @create 
 * @modify 
 * @desc [description]
 */


const app = require('../../app.js') 
const supertest = require('supertest')
const request = supertest(app)
const routes = require('../utils/routes')

it('gets the health endpoint', async done => {
  const response = await request.get(routes.BASE_ROUTE + routes.HEALTH_CHECK_ROUTE)
  expect(response.status).toBe(200)
  done()
})