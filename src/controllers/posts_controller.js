/**
 * @author Saurabh
 * @email saurabh.mishra@embibe.com
 * @create 
 * @modify 
 * @desc [description]
 */

const postsService = require('../services/posts_service')

const getRelatedPosts = async (req, res) => {
  try {
    const result = await postsService.getRelatedPosts(req.params.postId)
    if (result === undefined) {
      return res.sendStatus(400)
    }
    res.status(200).send(result)
  } catch (error) {
    console.error(error)
    res.sendStatus(400)
  }
}

module.exports = {
  getRelatedPosts
}
