/**
 * @author Saurabh
 * @email saurabh.mishra@embibe.com
 * @create 
 * @modify 
 * @desc [description]
 */

const db = require('../db').db;
const constants = require('../utils/constants')

// Fetch the selected post from the DB
const getPost = (postId) => {
  return new Promise((resolve, reject) => {
    db.query(constants.getPostQuery(postId), (err, results) => {
      if (err) {
        console.error(err)
        reject(err)
        return
      }
      if (results.length === 0) {
        reject('No such post')
        return
      }
      resolve(results[0])
    })
  })
}

// Fetch a limited number of posts from the DB
const getPosts = () => {
  return new Promise((resolve, reject) => {
    db.query(constants.getRelatedQuery(process.env.POST_QUERY_LIMIT), (err, results) => {
      if (err) {
        console.error(err)
        reject(err)
        return
      }
      resolve(results)
    })
  })
}

module.exports = {
  getPost,
  getPosts
}