/**
 * @author Saurabh
 * @email saurabh.mishra@embibe.com
 * @create 
 * @modify 
 * @desc [description]
 */


require('dotenv').config()

const mysql = require('mysql2')

const db = mysql.createPool({
  host: process.env.HOST_NAME,
  user: process.env.USERNAME,
  password: process.env.PASSWORD,
  database: process.env.DB
});

module.exports = {
  db
}